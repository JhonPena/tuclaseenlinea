<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Formulario de inscripcion</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
<!-- AQUI EMPIZA EL CSS -->   
    <style>
        h1{
            font-family: 'Pacifico', cursive;
            text-align: center;
            padding-top: 50px;
            color: firebrick;
            text-decoration-line: underline;
        }
        h2{
            font-family: 'Pacifico', cursive;
            text-align: center;
            color: firebrick;
            text-decoration-line: underline;
        }
        p{
            font-family: 'Abel', sans-serif;
            color:black;
            font-size-adjust: auto;
        }
        label{
            font-family: 'Abel', sans-serif;
            color:black;
            font-size-adjust: auto;
        }
        #btn{
            color:firebrick;
            background: transparent;
            border: 2px solid firebrick;
            border-radius: 6px;
        }
        #btn:hover{
            background-color: darkred;
            color: white;
        }
        input{
            border-radius: 4px;
            font-family: 'Abel', sans-serif;
        }
        select{
            border-radius: 4px;
            font-family: 'Abel', sans-serif;
        }
        body{
            text-align: center;
            background: rgb(120,180,58);
            background: linear-gradient(90deg, rgba(120,180,58,1) 0%, rgba(253,217,29,1) 50%, rgba(252,148,69,1) 100%);
        }
    </style>
<!-- AQUI TERMINA EL CSS -->
</head>

<body>
    <nav class="navbar navbar-expand-md fixed-top bg-danger navbar-dark" style="padding: 0px 10px 0px 10px;">
        <!-- la imagen o logo del menu  -->
        <a class="navbar-brand" href="#"> <img class="navbar-brand" src="" alt="" style="width: 6em;">
        </a>
        <!-- icono para el menu de dispositivos moviles -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!---->
        <!-- opciones de menu -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="TuClaseEnLinea.php" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acerca de
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="TuClaseEnLinea.php" id="30">Principal</a>
                        <a class="dropdown-item" href="#clase" id="301">Tu clase en linea</a>
                        <a class="dropdown-item" href="#beneficios" id="302">Beneficios</a>
                        <a class="dropdown-item" href="#soporte" id="301">Soporte y Mantenimiento</a>
                    </div>
                </li>
                <li class="nav-item" id="m2" onclick="activarclase(2)">
                    <a class="nav-link" href="login.php">Portal Web</a>
                </li>
                <li class="nav-item" id="m3" onclick="activarclase(3)">
                    <a class="nav-link" href="Formulario.php">Inscribete</a>
                </li>
               
            </ul>
        </div>
        <!-- fin del menu -->
    </nav>

    <div>

        <h1>Introduce la siguiente informacion:</h1>
        <p>A continuacion encontraras un formulario para poder<br />
            hacer parte de la familia <strong><em>TuClaseEnLinea</em></strong>.<br />
            No olvides llenar todos los espacios para poder <br />
            ponernos en contacto contigo lo antes posible.
        </p>

        <form action="Formulario.php" method="POST" name="registro" id="registro" onsubmit="return registrar();">

            <h2>Diligencia los datos personales: </h2>
            <p>Nombre: <input id="nombre" name="nombre" type="text"></p>
            <p>Apellido: <input id="apellido" name="apellido" type="text"></p>
            <p>Direccion: <input id="direccion" name="direccion" type="text"></p>
            <p>Correo: <input id="correo" name="correo" type="email" placeholder="nombre@hotmail.com"></p>
            <p>Telefono: <input id="telefono" name="telefono" type="tel"></p>

            <input id="btn" type="submit" name="enviar" value="Enviar" onclick="registrar()";>

        </form>
        <div id="registros">
    
    <?php  

    include("conect.php");

        if (isset($_POST['enviar'])){

            if(strlen($_POST['nombre']) >= 1 && strlen($_POST['apellido']) >= 1 && strlen($_POST['direccion']) >= 1 && strlen($_POST['correo']) >= 1 && strlen($_POST['telefono']) >= 1){

            $nombre = trim($_POST['nombre']);
            $apellido = trim($_POST['apellido']);
            $direccion = trim($_POST['direccion']);
            $correo = trim($_POST['correo']);
            $telefono = trim($_POST['telefono']);

            $sql ="INSERT INTO registros(nombre, apellido, direccion, correo, telefono) VALUES ('$nombre','$apellido','$direccion','$correo','$telefono')";

            $resultado = mysqli_query($conect, $sql);

            if($resultado){
                echo '¡Gracias!';
            }else{
                echo '¡error!';
            }
        }
    }
    ?>
        </div>
        <br>
        <p>Para regresar a la pagina anterior <a href="TuClaseEnLinea.html"><strong>¡Click Aqui!</strong></a></p>
    </div>
</body>

<hr>
<footer>
    <div class="container-fluid bg-danger">
        <!-- pie de pagina de la pagina -->
        <br>
        <p class="text-center">
            Creado por: Tu clase en linea
            <br>
            &copy; 2020
        </p>

        <br>
    </div>

</footer>

</html>

<script>
    $(document).ready(function () {
        var dia = new Date();
        var hora = dia.getHours();

        function saludo(horas) {
            while (horas <= 12 && horas >= 0) {
                alert("Buenos días!!");
                horas = 24;
            };
            while (horas >= 13 && horas <= 18) {
                alert("Buenas tardes!!");
                horas = 24;
            };
            while (horas >= 19 && horas <= 23) {
                alert("Buenas noches!!");
                horas = 24;
            };
        };
        saludo(hora);

    });

    function registrar() {
        var nombre = $("#nombre").val();
        var apellido = $("#apellido").val();
        var direccion = $("#direccion").val();
        var correo = $("#correo").val();
        var telefono = $("#telefono").val();


        if (nombre != "" && apellido != ""
            && direccion != "" && grado != "" && correo != ""
            && telefono != "") {

            alert("SE HA REGISTRADO EN EL SISTEMA");
            registro = true;
        }
        else {

            alert("POR FAVOR LLENAR TODOS LOS DATOS");
            registro = false;
        }
        return registro;
    }

</script>