<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<!-- AQUI EMPIEZA EL CSS -->        
    <style>
        h1{
            font-family: 'Pacifico', cursive;
            text-align: center;
            padding-top: 50px;
            color: firebrick;
            text-decoration-line: underline;
        }
        p{
            font-family: 'Abel', sans-serif;
            color:black;
            font-size-adjust: auto;
        }
        body{
            background: rgb(120,180,58);
            background: linear-gradient(90deg, rgba(120,180,58,1) 0%, rgba(253,217,29,1) 50%, rgba(252,148,69,1) 100%);
        }
        #boton{
            color:firebrick;
            background: transparent;
            border: 2px solid firebrick;
            border-radius: 6px;
        }
        #boton:hover{
            background-color: darkred;
            color: white;
        }
        #in{
            font-family: 'Pacifico', cursive;
            color: firebrick;
            background-color: lightgray;
        }
        select{
            font-family: 'Abel', sans-serif;
            color:black;
        }
        input{
            font-family: 'Abel', sans-serif;
            color: black;
            border-radius: 4px;
            background-color: rgba(0,0,0,0.3);
        }
        #registro{
            font-family: 'Abel', sans-serif;
            color: darkturquoise;
        }
        #pie{
            background-color: lightgray;
        }
        button{
            color:firebrick;
            background: transparent;
            border: 2px solid firebrick;
            border-radius: 6px;
        }
        button:hover{
            background-color: darkred;
            color: white;
        }
    </style>
<!-- AQUI TERMINA EL CSS -->
   
    </head>
<body>
    <nav class="navbar navbar-expand-md fixed-top bg-danger navbar-dark" style="padding: 0px 10px 0px 10px;">
        <!-- la imagen o logo del menu  -->
        <a class="navbar-brand" href="#"> <img class="navbar-brand" src="" alt="" style="width: 6em;">
        </a>
        <!-- icono para el menu de dispositivos moviles -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!---->
        <!-- opciones de menu -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">

            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="TuClaseEnLinea.php" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acerca de
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="TuClaseEnLinea.php" id="30">Principal</a>
                        <a class="dropdown-item" href="#clase" id="301">Tu clase en linea</a>
                        <a class="dropdown-item" href="#beneficios" id="302">Beneficios</a>
                        <a class="dropdown-item" href="#soporte" id="301">Soporte y Mantenimiento</a>
                    </div>
                </li>
                <li class="nav-item" id="m2" onclick="activarclase(2)">
                    <a class="nav-link" href="login.php">Portal Web</a>
                </li>
                <li class="nav-item" id="m3" onclick="activarclase(3)">
                    <a class="nav-link" href="Formulario.html">Inscribete</a>
                </li>
                
            </ul>
        </div>
        <!-- fin del menu -->
    </nav>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <br>
        <br>
        <br>
        <div class="row justify-content-center">
            <center>
                <form action="index.php" method="POST" style="padding-top: 50px;">
                    <h1>Iniciar Sesion</h1>
                    <p><strong>Nombre de usuario</strong></p>
                    <input type="text" name="username">
                    <br><br>
                    <p><strong>Contraseña</strong></p>
                    <input type="password" name="clave">
                    <br> <br>
                    <button type="submit">Ingresar</button>
                </form>
            </center>
        </div>
    </div>
</body>
<footer style=" position:fixed;left:0px;bottom:0px;height:10%;width:100%;">
    <div class="container-fluid bg-danger">
        <!-- pie de pagina de la pagina -->
        <br>
        <p class="text-center">
            Creado por: Tu clase en linea
            <br>
            &copy; 2020
        </p>
        <br>
    </div>
</footer>

</html>
