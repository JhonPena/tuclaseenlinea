<?php  

session_start();
$username = $_SESSION['username'];

?>
<?php  
    
    $usuario = 'root';
    $contraseña = 'jfpmean135';
    $host = 'localhost';
    $base = 'tuclase';

    try{
        $connection = new PDO('mysql:host='.$host.';dbname='.$base, $usuario, $contraseña);
    }
    catch(PDOException $e){
        print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Portal Web</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet">    
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
            <style>
            h1{
                font-family: 'Pacifico', cursive;
                color: firebrick;
                padding-top: 50px;
                text-align: center;
            }
            p{
                font-family: 'Abel', sans-serif;
                color: black;
                font-size-adjust: auto;
                text-align: center;
            }
             ul li{
                font-family: 'Abel', cursive;
                color: black;
                font-size-adjust: auto;
            }
            body{
                background: rgb(120,180,58);
                background: linear-gradient(90deg, rgba(120,180,58,1) 0%, rgba(253,217,29,1) 50%, rgba(252,148,69,1) 100%);
            }
            table{
                border-style: solid;
                border-color: black;
                margin: auto;
            }
            th{
                border-style: solid;
                border-color: black;
                background: rgba(255,150,190, 0.4);
                font-family: 'Pacifico', cursive;
            }
            td{
                border-style: solid;
                border-color: black;
                background: rgba(13,165,240,0.4);
                font-family: 'Abel', sans-serif;
            }
    </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-md fixed-top bg-danger navbar-dark" style="padding: 0px 10px 0px 10px;">
            <!-- la imagen o logo del menu  -->
            <a class="navbar-brand" href="#"> <img class="navbar-brand" src="" alt="" style="width: 6em;">
            </a>
            <!-- icono para el menu de dispositivos moviles -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!---->
            <!-- opciones de menu -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto text-center">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="TuClaseEnLinea.php" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Acerca de
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="TuClaseEnLinea.php" id="30">Principal</a>
                            <a class="dropdown-item" href="#clase" id="301">Tu clase en linea</a>
                            <a class="dropdown-item" href="#beneficios" id="302">Beneficios</a>
                            <a class="dropdown-item" href="#soporte" id="301">Soporte y Mantenimiento</a>
                        </div>
                    </li>
                    <li class="nav-item" id="m2" onclick="activarclase(2)">
                        <a class="nav-link" href="login.php">Portal Web</a>
                    </li>
                    <li class="nav-item" id="m3" onclick="activarclase(3)">
                        <a class="nav-link" href="Formulario.html">Inscribete</a>
                    </li>
                    <li class="nav-item" id="m5">
                        <?php echo "<a class='nav-link' href='exit.php'>Cerrar Sesion</a>" ?>
                    </li>
                </ul>
            </div>
        </nav>
                    <!-- fin del menu -->
        <div>
            <h1>Bienvenido a Tu Clase En Linea</h1>
            <p>A continuacion encontraras informacion actualizada sobre<br>
               los estudiantes miembros de tu colegio, con su respectivo<br>
               curso, profesor, acudiente y materias que le corresponden<br>
               a cada uno de ellos segun su curso asignado.</p>
        </div>
        <section>
            <table class="">
                <tr class="">
                    <th class="">ID</th>
                    <th class="">Nombre</th>
                    <th class="">Apellido</th>
                    <th class="">Profesor</th>
                    <th class="">Curso</th>
                    <th class="">Materia</th>
                    <th class="">Acudiente</th>
                </tr>

                <?php  

                $query = "SELECT Es.id_estudiante,Es.nombre, Es.apellido, Cur.nombre_cur, Pro.nombre_prof, Mat.nombre_mat, Acu.nombre_acu 
                                FROM estudiante Es
                                INNER JOIN profesores Pro ON Es.idprofesor = Pro.id_profesor

                                INNER JOIN curso Cur ON Es.idcurso = Cur.id_curso 
                                
                                INNER JOIN materias Mat ON Es.idmateria = Mat.id_materias

                                INNER JOIN acudiente Acu ON Es.idacudiente = Acu.id_acudiente";
                    $cons=$connection->query($query);
                    while ($row=$cons->fetch(PDO::FETCH_ASSOC))
                    {
                        echo '
                        <tr>
                            <td>'.$row['id_estudiante'].'</td>
                            <td>'.$row['nombre'].'</td>
                            <td>'.$row['apellido'].'</td>
                            <td>'.$row['nombre_prof'].'</td>
                            <td>'.$row['nombre_cur'].'</td>
                            <td>'.$row['nombre_mat'].'</td>
                            <td>'.$row['nombre_acu'].'</td>
                        </tr>';
                    }

                ?>
            </table>
        </section>
    </body>
    <footer style=" position:fixed;left:0px;bottom:20px;height:10%;width:100%;">
    <div class="container-fluid bg-danger">
        <!-- pie de pagina de la pagina -->
        <br>
        <p class="text-center">
            Creado por: Tu clase en linea
            <br>
            &copy; 2020
        </p>
        <br>
    </div>
</footer>
</html>